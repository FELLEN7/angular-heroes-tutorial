import { trigger, transition, style, animate, state } from '@angular/animations';

export let fade = trigger('fade', [
  state('void', style({opacity: 0, height: '0em', margin: '0em .5em 0 .5em', padding: '0em'})),
  transition('void => *', [
    animate(200),
  ]),
  transition('* => void', [
    animate(200)
  ])
]);


export let drop = trigger('drop', [
  transition(':enter', [
    style({maxHeight: '0px', margin: '0em'}),
    animate(500, style({maxHeight: '500px'}))
  ])
])